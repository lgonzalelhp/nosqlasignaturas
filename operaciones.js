var admin = require("firebase-admin");
var serviceAccount = require("./serviceAccountKey.json");

var dbRef = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://ribw-d7c3f.firebaseio.com"
}).firestore();

var FieldValue = require('firebase-admin').firestore.FieldValue;

var asignaturasRef = dbRef.collection("asignaturas");

function operacionSet() {
    var IW = {
        nombre : "Ingeniería Web",
        profesor :"Juan Carlos Preciado",
        semestre : 8
    }
    asignaturasRef.doc('IW').set(IW).then(function(){
        console.log("Registro IW salvado");
    })
    .catch(function(){
        console.log("Fallo al salvar IW")
    });

    var RIBW = {
        nombre : "Recuperación de la Informacion en Busquedas Web",
        profesor :"Felix Rodriguez",
        departamento : "Ingeniería de sistemas informáticos y telemáticos",
        semestre : 8
    }
    asignaturasRef.doc('RIWB').set(RIBW).then(function(){
        console.log("Registro RIBW salvado");
    })
    .catch(function(){
        console.log("Fallo al salvar RIBW")
    });
}

function operacionGet() {
    asignaturasRef.get()
    .then(asignaturas => {
        asignaturas.forEach(asignatura => {
            console.log(asignatura.data());
        });
    })
    .catch (error => {
        console.log(error);
    })
}


function operacionDeleteSemestre() {
    asignaturasRef.doc('IW').update({
        semestre: FieldValue.delete()
    }).then(function(){
        console.log("Registro IW semestre borrado");
    })
    .catch(function(){
        console.log("Fallo al borrar el semestre en el registro IW")
    });
}

function operacionDeleteIW() {
    asignaturasRef.doc('IW').delete()
    .then(function(){
        console.log("Registro IW borrado");
    })
    .catch(function(){
        console.log("Fallo al borrar el registro IW")
    });
}

exports.operacionSet = operacionSet;
exports.operacionGet = operacionGet;
exports.operacionDeleteSemestre = operacionDeleteSemestre;
exports.operacionDeleteIW = operacionDeleteIW;